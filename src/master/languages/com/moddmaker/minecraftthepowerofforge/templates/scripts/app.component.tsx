import { AfterViewInit, Component, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import React from "react";
import ReactDOM from "react-dom";
import react from './components/react';
@Component({
  selector: 'app-root',
  template: '<div [id]="rootId"></div>',
  styleUrls: ['./../styles/app.component.css']
})

export class AppComponent implements OnChanges, AfterViewInit, OnDestroy{
  title = 'angularreactapp';

  public rootId = 'rootId'

  ngOnChanges(changes: SimpleChanges){
    this.render();
  }

  ngAfterViewInit(){
    this.render();
  }

  ngOnDestroy(){

  }

  private render(){
    ReactDOM.render(React.createElement(react ), document.getElementById(this.rootId));
  }
}
