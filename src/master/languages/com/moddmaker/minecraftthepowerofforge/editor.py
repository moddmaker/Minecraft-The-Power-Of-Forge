"""python modules packages and imports"""
from flask import Flask, render_template

# pls add """ pls this is vary imported
# pls use """ in the to line 1 of your file

app = Flask(__name__)

@app.route("/")
def html():
    """website home page settings with html"""
    print("html has been updated")
    return render_template("index.html")
# without """ you will get error in pylint code scanning
# pls always use """ after every like this

@app.route("/php")
def php():
    """website home page settings with php"""
    print("website has been updated")
    return render_template("Website.php")

if __name__ == "__main__":
    app.run(debug=True)
