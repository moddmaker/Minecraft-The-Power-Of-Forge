package com.moddmaker.minecraftthepowerofforge;
import java.io.Serializable.*;
import java.util.Vector.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.io.*;
import javax.script.*; 

public class MinecraftThePowerOfForge{ 
    private static Logger = logger;
    public static final String modid = ExampleMod.MODID;
    public static final String name = ExampleMod.NAME;
    public static final String version = ExampleMod.VERSION;
    public static final String MODID = "minecraftthepowerofforge";
    public static final String NAME = "Minecraft The Power Of Forge";
    public static final String VERSION = "1.0.0";

    public void preInit(FMLPreInitializationEvent event)
    {
        public static final String logger = event.getModLog();
    }

    public void init(FMLInitializationEvent event)
    {
        // some example code
        logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
    }
    public static void callRuby(){

      ScriptEngine jruby = new ScriptEngineManager().getEngineByName("jruby");
        //process a ruby file
        try {           
		jruby.eval(new BufferedReader(new FileReader("ruby.rb")));
             jruby.put("a", "2");
             jruby.put("b", "3");   
             System.out.println("result: " +jruby.get("res"));

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ScriptException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

	}

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(4343, 10);
        Socket socket = serverSocket.accept();
        InputStream is = socket.getInputStream();
        OutputStream os = socket.getOutputStream();

        // Receiving
        byte[] lenBytes = new byte[4];
        is.read(lenBytes, 0, 4);
        int len = (((lenBytes[3] & 0xff) << 24) | ((lenBytes[2] & 0xff) << 16) |
                  ((lenBytes[1] & 0xff) << 8) | (lenBytes[0] & 0xff));
        byte[] receivedBytes = new byte[len];
        is.read(receivedBytes, 0, len);
        String received = new String(receivedBytes, 0, len);

        System.out.println("Server received: " + received);

        // Sending
        String toSend = "Echo: " + received;
        byte[] toSendBytes = toSend.getBytes();
        int toSendLen = toSendBytes.length;
        byte[] toSendLenBytes = new byte[4];
        toSendLenBytes[0] = (byte)(toSendLen & 0xff);
        toSendLenBytes[1] = (byte)((toSendLen >> 8) & 0xff);
        toSendLenBytes[2] = (byte)((toSendLen >> 16) & 0xff);
        toSendLenBytes[3] = (byte)((toSendLen >> 24) & 0xff);
        os.write(toSendLenBytes);
        os.write(toSendBytes);

        socket.close();
        serverSocket.close();
    }
    public static void python(String[] args)
	{
	   try
	   {

        String command = "python animation.py";
        Process p = Runtime.getRuntime().exec(command);
        p.waitFor();
        BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
          String line;
         while ((line = bri.readLine()) != null)
         {
            System.out.println(line);
         }
          bri.close();
          while ((line = bre.readLine()) != null)
          {
            System.out.println(line);
          }
          bre.close();
          p.waitFor();
          System.out.println("Done.");
          p.destroy();
     }
          catch(Exception e)
     {
        System.out.println(e);
     }
   }
}
