# -*- encoding: utf-8 -*-
# stub: cxxproject 0.6.31 ruby lib

Gem::Specification.new do |s|
  s.name = "cxxproject".freeze
  s.version = "0.6.31"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["oliver mueller".freeze]
  s.date = "2013-11-14"
  s.description = "    Some more high level building blocks for cpp projects.\n".freeze
  s.email = "oliver.mueller@gmail.com".freeze
  s.homepage = "https://github.com/marcmo/cxxproject".freeze
  s.licenses = ["BSD-2".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "Cpp Support for Rake.".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<colored>.freeze, [">= 0"])
    s.add_runtime_dependency(%q<frazzle>.freeze, [">= 0"])
  else
    s.add_dependency(%q<colored>.freeze, [">= 0"])
    s.add_dependency(%q<frazzle>.freeze, [">= 0"])
  end
end
