# -*- encoding: utf-8 -*-
# stub: cxx 0.1.17 ruby lib

Gem::Specification.new do |s|
  s.name = "cxx".freeze
  s.version = "0.1.17"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["christian koestlin".freeze]
  s.date = "2016-08-22"
  s.description = "ruby-based dsl for cxxproject".freeze
  s.email = "christian.koestlin@gmail.com".freeze
  s.executables = ["cxx".freeze]
  s.files = ["bin/cxx".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "defines the method cxx_configuration".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<cxxproject>.freeze, [">= 0"])
    s.add_runtime_dependency(%q<highline>.freeze, [">= 0"])
  else
    s.add_dependency(%q<cxxproject>.freeze, [">= 0"])
    s.add_dependency(%q<highline>.freeze, [">= 0"])
  end
end
