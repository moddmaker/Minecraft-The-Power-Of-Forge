# -*- encoding: utf-8 -*-
# stub: frazzle 0.1.4 ruby lib

Gem::Specification.new do |s|
  s.name = "frazzle".freeze
  s.version = "0.1.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Kevin russ".freeze]
  s.date = "2016-02-07"
  s.description = "plugin registry based on gems".freeze
  s.email = "kevin.russ@esrlabs.com".freeze
  s.extra_rdoc_files = ["README.md".freeze]
  s.files = ["README.md".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "gem plugin-manager using extention-points".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version
end
