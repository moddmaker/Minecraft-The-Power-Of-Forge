# -*- encoding: utf-8 -*-
# stub: rake-dotnet 0.1.20 ruby lib

Gem::Specification.new do |s|
  s.name = "rake-dotnet".freeze
  s.version = "0.1.20"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Peter MouncePeter Mounce".freeze]
  s.date = "2009-11-30"
  s.description = "Making a .NET build-automation dev's life easier, one angle-bracket at a time".freeze
  s.email = "public@neverrunwithscissors.compublic@neverrunwithscissors.com".freeze
  s.extra_rdoc_files = ["History.txt".freeze, "Manifest.txt".freeze, "README.txt".freeze]
  s.files = ["History.txt".freeze, "Manifest.txt".freeze, "README.txt".freeze]
  s.homepage = "http://blog.neverrunwithscissors.com/tag/rake-dotnet".freeze
  s.rdoc_options = ["--main".freeze, "README.txt".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "Build automation for .NET builds".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<rake>.freeze, [">= 0.8.3"])
    s.add_development_dependency(%q<hoe>.freeze, [">= 2.3.3"])
  else
    s.add_dependency(%q<rake>.freeze, [">= 0.8.3"])
    s.add_dependency(%q<hoe>.freeze, [">= 2.3.3"])
  end
end
