# -*- encoding: utf-8 -*-
# stub: tracer 0.1.1 ruby lib

Gem::Specification.new do |s|
  s.name = "tracer".freeze
  s.version = "0.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Keiju ISHITSUKA".freeze]
  s.bindir = "exe".freeze
  s.date = "2020-12-22"
  s.description = "Outputs a source level execution trace of a Ruby program.".freeze
  s.email = ["keiju@ruby-lang.org".freeze]
  s.homepage = "https://github.com/ruby/tracer".freeze
  s.licenses = ["Ruby".freeze, "BSD-2-Clause".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "Outputs a source level execution trace of a Ruby program.".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version
end
