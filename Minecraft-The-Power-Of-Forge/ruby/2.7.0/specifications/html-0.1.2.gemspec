# -*- encoding: utf-8 -*-
# stub: html 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "html".freeze
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Markus Schirp".freeze]
  s.date = "2014-03-29"
  s.description = "Because HTML is not a String".freeze
  s.email = ["mbj@schirp-dso.com".freeze]
  s.extra_rdoc_files = ["TODO".freeze]
  s.files = ["TODO".freeze]
  s.homepage = "https://github.com/mbj/html".freeze
  s.licenses = ["MIT".freeze]
  s.rubygems_version = "3.1.2".freeze
  s.summary = "Because HTML is not a String".freeze

  s.installed_by_version = "3.1.2" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<adamantium>.freeze, ["~> 0.2.0"])
    s.add_runtime_dependency(%q<equalizer>.freeze, ["~> 0.0.9"])
  else
    s.add_dependency(%q<adamantium>.freeze, ["~> 0.2.0"])
    s.add_dependency(%q<equalizer>.freeze, ["~> 0.0.9"])
  end
end
