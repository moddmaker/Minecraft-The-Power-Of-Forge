I"\%<p>Remember <a href="/minecraft-the-power-of-forge/ruby/2.7.0/gems/chunky_png-1.4.0/docs/2010/01/14/memory-efficiency-when-using-ruby.html">my last post</a>, where I representing a pixel with a Fixnum, storing the R, G, B and A value in its 4 bytes of memory? Well, I have been working some more on <a href="https://github.com/wvanbergen/chunky_png">my PNG library</a> and I am now trying loading and saving an image.</p>

<p>Using the <a href="https://www.w3.org/TR/PNG/">PNG specification</a>, building a PNG encoder/decoder isn’t that hard, but the required algorithmic calculations make sure that performance in Ruby is less than stellar. I have rewritten all calculations to only use fast integer math (plus, minus, multiply and bitwise operators), but simply the amount of code that is getting executed is slowing Ruby down. What more can I do to improve the performance?</p>

<h2 id="encoding-rgba-images">Encoding RGBA images</h2>

<p>Optimizing loading images is very hard, because PNG images can have many variations, and taking shortcuts means that some images are no longer supported. Not so with saving images: as long an image is saved using one of the valid variations, every PNG decoder will be able to read the file. Let’s see if it is possible to optimize one of these encoding variations.</p>

<p>During encoding, the image get splits up into scanlines (rows) of pixels, which in turn get converted into bytes. These bytes can be filtered for optimal compression. For a 3×3 8-bit RGBA image, the result looks like this:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>F Rf Gf Bf Af Rf Gf Bf Af Rf Gf Bf Af
F Rf Gf Bf Af Rf Gf Bf Af Rf Gf Bf Af
F Rf Gf Bf Af Rf Gf Bf Af Rf Gf Bf Af
</code></pre></div></div>

<p>Every line starts with a byte F indicating the filter method, followed by the filtered R, G and B value for every pixel on that line. Now, if we choose filter method 0, which means no filtering, the result looks like this:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>0 Ro Go Bo Ao Ro Go Bo Ao Ro Go Bo Ao
0 Ro Go Bo Ao Ro Go Bo Ao Ro Go Bo Ao
0 Ro Go Bo Ao Ro Go Bo Ao Ro Go Bo Ao
</code></pre></div></div>

<p>Now, the original R, G, B and A byte from the original pixel’s Fixnum, occur in <a href="https://en.wikipedia.org/wiki/Endianness">big-endian or network byte order</a>, starting with the top left pixel, moving left to right and then top to bottom. Exactly like the pixels are stored in our image’s pixel array! This means that we can use the Array#pack method to encode into this format. The Array#pack-notation for this is “xN3” in which x get translated into a null byte, and every N as 4-byte integer in network byte order. For optimal performance, it is best to not split the original array in lines, but to pack the complete pixel array at once. So, we can encode all pixels with this command:</p>

<figure class="highlight"><pre><code class="language-ruby" data-lang="ruby"><span class="n">pixeldata</span> <span class="o">=</span> <span class="n">pixels</span><span class="p">.</span><span class="nf">pack</span><span class="p">(</span><span class="s2">"xN</span><span class="si">#{</span><span class="n">width</span><span class="si">}</span><span class="s2">"</span> <span class="o">*</span> <span class="n">height</span><span class="p">)</span></code></pre></figure>

<p>This way, the splitting the image into lines, splitting the pixels into bytes, and filtering the bytes can be skipped. In Ruby 1.8.7, this means a speedup of over 1500% (no typo)! Of course, because no filtering applied, the subsequent compression is not optimal, but that is a tradeoff that I am willing to make.</p>

<h2 id="encoding-rgb-images">Encoding RGB images</h2>

<p>What about RGB images without alpha channel? We can simply choose to encode these using the RGBA method, but that increases the file size with roughly 25%. Can we fix this somehow?</p>

<p>The unfiltered pixel data should look something like this:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>0 Ro Go Bo Ro Go Bo Ro Go Bo
0 Ro Go Bo Ro Go Bo Ro Go Bo
0 Ro Go Bo Ro Go Bo Ro Go Bo
</code></pre></div></div>

<p>This means that for every pixel that is encoded as a 4-byte integer, the last byte should be ditched. Luckily, the <code class="language-plaintext highlighter-rouge">Array#pack</code> method offers a modifier that does just that: <code class="language-plaintext highlighter-rouge">X</code>. Packing a 3 pixel line can be done with <code class="language-plaintext highlighter-rouge">"xNXNXNX"</code>. Again we would like to pack the whole pixel array at once:</p>

<figure class="highlight"><pre><code class="language-ruby" data-lang="ruby"><span class="n">pixeldata</span> <span class="o">=</span> <span class="n">pixels</span><span class="p">.</span><span class="nf">pack</span><span class="p">((</span><span class="s2">"x"</span> <span class="o">+</span> <span class="p">(</span><span class="s1">'NX'</span> <span class="o">*</span> <span class="n">width</span><span class="p">))</span> <span class="o">*</span> <span class="n">height</span><span class="p">)</span></code></pre></figure>

<p>Because all the encoding steps can get skipped once again, the speed improvement is again 1500%! And the result is 25% smaller than the RGBA method. This method is actually so speedy, that saving an image using Ruby 1.9.1 is only a little bit slower (&lt; 10%) than saving a PNG image using RMagick! See my <a href="https://github.com/wvanbergen/chunky_png/wiki/performance-comparison">performance comparison</a>.</p>

<h2 id="loading-image">Loading image</h2>

<p>Given the promising results of the Array#pack method, using its counterpart String#unpack looks promising for speedy image loading, if you know the image’s size and the encoding format beforehand.</p>

<p>An RGBA formatted stream can be loaded quickly with this command:</p>

<figure class="highlight"><pre><code class="language-ruby" data-lang="ruby"><span class="n">pixels</span> <span class="o">=</span> <span class="n">rgba_pixeldata</span><span class="p">.</span><span class="nf">unpack</span><span class="p">(</span><span class="s2">"N</span><span class="si">#{</span><span class="n">width</span> <span class="o">*</span> <span class="n">height</span><span class="si">}</span><span class="s2">"</span><span class="p">)</span>
<span class="n">image</span> <span class="o">=</span> <span class="no">Image</span><span class="p">.</span><span class="nf">new</span><span class="p">(</span><span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">,</span> <span class="n">pixels</span><span class="p">)</span></code></pre></figure>

<p>For an RGB formatted stream, we can use the X modifier again, but we have to make sure to set the alpha value for every pixel to 255:</p>

<figure class="highlight"><pre><code class="language-ruby" data-lang="ruby"><span class="n">pixels</span> <span class="o">=</span> <span class="n">rgb_pixeldata</span><span class="p">.</span><span class="nf">unpack</span><span class="p">(</span><span class="s2">"NX"</span> <span class="o">*</span> <span class="p">(</span><span class="n">width</span> <span class="o">*</span> <span class="n">height</span><span class="p">))</span>
<span class="n">pixels</span><span class="p">.</span><span class="nf">map!</span> <span class="p">{</span> <span class="o">|</span><span class="n">pixel</span><span class="o">|</span> <span class="n">pixel</span> <span class="o">|</span> <span class="mh">0x000000ff</span> <span class="p">}</span>
<span class="n">image</span> <span class="o">=</span> <span class="no">Image</span><span class="p">.</span><span class="nf">new</span><span class="p">(</span><span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">,</span> <span class="n">pixels</span><span class="p">)</span></code></pre></figure>

<p>You can even use little-endian integers to load streams in ABGR format!</p>

<figure class="highlight"><pre><code class="language-ruby" data-lang="ruby"><span class="n">pixels</span> <span class="o">=</span> <span class="n">abgr_pixeldata</span><span class="p">.</span><span class="nf">unpack</span><span class="p">(</span><span class="s2">"V</span><span class="si">#{</span><span class="n">width</span> <span class="o">*</span> <span class="n">height</span><span class="si">}</span><span class="s2">"</span><span class="p">)</span>
<span class="n">image</span> <span class="o">=</span> <span class="no">Image</span><span class="p">.</span><span class="nf">new</span><span class="p">(</span><span class="n">width</span><span class="p">,</span> <span class="n">height</span><span class="p">,</span> <span class="n">pixels</span><span class="p">)</span></code></pre></figure>

<p>Loading pixel data for an image like this is again over 1500% faster than decoding the same PNG image. However, this can only be applied if you have control over the input format of the image.</p>

<h2 id="to-conclude">To conclude</h2>

<p><code class="language-plaintext highlighter-rouge">Array#pack</code> and <code class="language-plaintext highlighter-rouge">String#unpack</code> really have increased the performance for my code. If you can apply them for project, don’t hesitate and spread the love! For all other cases, use as little code as possible, and upgrade to Ruby 1.9 for improved algorithmic performance.</p>
:ET